#!/bin/bash -e
name=fncntrl
here=$(realpath "$(dirname "$0")")

cat > "$here/$name.service" <<EOF
[Unit]
Description=fan control

[Service]
WorkingDirectory=$here
ExecStart=/usr/bin/python "$here/fncntrl.py" run "$here/config"

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl link "$here/$name.service"
sudo systemctl enable --now "$name.service"
