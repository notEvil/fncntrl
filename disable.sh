#!/bin/bash -e
name=fncntrl

sudo systemctl stop "$name.service"
sudo systemctl disable "$name.service"
