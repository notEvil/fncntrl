import numpy
import scipy.interpolate as s_interpolate

try:
    import pynvml

except ImportError:
    pynvml = None

import abc
import collections
import contextlib
import ctypes
import itertools
import logging
import math
import pathlib
import time
import threading
import typing


MANUAL_MODE = 1


def one(objects):
    (object,) = objects
    return object


class Fan(abc.ABC):
    @abc.abstractmethod
    def get_pwm_enable(self) -> typing.Any: ...

    @abc.abstractmethod
    def set_pwm_enable(self, value: typing.Any): ...

    @abc.abstractmethod
    def get_pwm(self) -> int: ...

    @abc.abstractmethod
    def set_pwm(self, value: int): ...

    @abc.abstractmethod
    def get_speed(self) -> int: ...


class Sensor(abc.ABC):
    @abc.abstractmethod
    def get_temperature(self) -> float: ...


@contextlib.contextmanager
def pwm_enable(fan, value):
    previous_value = fan.get_pwm_enable()
    fan.set_pwm_enable(value)
    try:
        yield

    finally:
        fan.set_pwm_enable(previous_value)


class SensorsGroup:
    @staticmethod
    def get_all():
        return map(SensorsGroup, pathlib.Path("/sys/class/hwmon").iterdir())

    def __init__(self, path):
        super().__init__()

        self.path = path

    def get_id(self):
        return f"sensors:{self.get_name()}"

    def get_name(self):
        with open(self.path / "name", "r") as file:
            return file.read().strip()

    def get_sensors(self):
        for number in itertools.count(1):
            path = self.path / f"temp{number}_label"
            if not path.exists():
                break

            yield SensorsSensor(number, self)

    def get_fans(self):
        for number in itertools.count(1):
            is_pwm = True
            speed_path = self.path / f"fan{number}_target"
            if not speed_path.exists():
                is_pwm = False
                speed_path = self.path / f"fan{number}_input"
                if not speed_path.exists():
                    break

            yield SensorsFan(number, is_pwm, speed_path, self)


class SensorsSensor(Sensor):
    def __init__(self, number, sensors_group):
        super().__init__()

        self.number = number
        self.sensors_group = sensors_group

        self._temperature_file = _File(sensors_group.path / f"temp{number}_input")

    def get_id(self):
        return f"{self.sensors_group.get_id()}:{self.get_name()}"

    def get_name(self):
        with open(self.sensors_group.path / f"temp{self.number}_label", "r") as file:
            return file.read().strip()

    def get_temperature(self):
        return int(self._temperature_file.get().strip()) / 1000


class SensorsFan(Fan):
    def __init__(self, number, is_pwm, speed_path, sensors_group):
        super().__init__()

        self.number = number
        self.is_pwm = is_pwm
        self.speed_path = speed_path
        self.sensors_group = sensors_group

        self._speed_file = _File(speed_path)
        self._enable_file = _File(sensors_group.path / f"pwm{number}_enable")
        self._pwm_file = _File(sensors_group.path / f"pwm{number}") if is_pwm else None

    def get_id(self):
        return f"{self.sensors_group.get_id()}:fan{self.number}"

    def get_speed(self):
        return int(self._speed_file.get().strip())

    def get_pwm_enable(self):
        assert self.is_pwm
        return int(self._enable_file.get().strip())

    def set_pwm_enable(self, value):
        assert self.is_pwm
        self._enable_file.set(str(value))

    def get_pwm(self):
        assert self.is_pwm
        return int(self._pwm_file.get().strip())

    def set_pwm(self, value):
        assert self.is_pwm
        self._pwm_file.set(str(value))


class _File:
    def __init__(self, path):
        super().__init__()

        self.path = path

        self._lock = threading.Lock()
        self._get_file = None
        self._set_file = None

    def get(self):
        with self._lock:
            if self._get_file is None:
                self._get_file = open(self.path, "r")

            self._get_file.seek(0)
            return self._get_file.read()

    def set(self, string):
        with self._lock:
            if self._set_file is None:
                self._set_file = open(self.path, "w")

            self._set_file.seek(0)
            self._set_file.write(string)
            self._set_file.flush()


_nvml_initialized = False


def get_nvml():
    global _nvml_initialized
    assert pynvml is not None
    if not _nvml_initialized:
        pynvml.nvmlInit()
        _nvml_initialized = True

    return pynvml


class NvidiaDevice(Sensor):
    @staticmethod
    def get_all():
        for index in range(get_nvml().nvmlDeviceGetCount()):
            yield NvidiaDevice(get_nvml().nvmlDeviceGetHandleByIndex(index))

    def __init__(self, device_handle):
        super().__init__()

        self.device_handle = device_handle

    def get_id(self):
        return f"nvidia:{self.get_name()}"

    def get_name(self):
        return get_nvml().nvmlDeviceGetName(self.device_handle)

    def get_temperature(self):
        _v_ = get_nvml().NVML_TEMPERATURE_GPU
        return get_nvml().nvmlDeviceGetTemperature(self.device_handle, _v_)

    def get_fans(self):
        for index in range(get_nvml().nvmlDeviceGetNumFans(self.device_handle)):
            yield NvidiaFan(index, self)


class NvidiaFan(Fan):
    def __init__(self, index, nvidia_device):
        super().__init__()

        self.index = index
        self.nvidia_device = nvidia_device

    def get_id(self):
        return f"{self.nvidia_device.get_id()}:fan{self.index}"

    def get_pwm_enable(self):
        policy = ctypes.c_uint()

        _v_ = get_nvml().nvmlDeviceGetFanControlPolicy_v2(
            self.nvidia_device.device_handle, self.index, ctypes.pointer(policy)
        )
        assert _v_ == get_nvml().NVML_SUCCESS

        return policy.value

    def set_pwm_enable(self, value):
        _v_ = {MANUAL_MODE: get_nvml().NVML_FAN_POLICY_MANUAL}.get(value, value)
        _v_ = get_nvml().nvmlDeviceSetFanControlPolicy(
            self.nvidia_device.device_handle, self.index, _v_
        )
        assert _v_ == get_nvml().NVML_SUCCESS

    def get_pwm(self):
        _v_ = self.nvidia_device.device_handle
        return get_nvml().nvmlDeviceGetFanSpeed_v2(_v_, self.index)

    def set_pwm(self, value):
        _v_ = self.nvidia_device.device_handle
        _v_ = get_nvml().nvmlDeviceSetFanSpeed_v2(_v_, self.index, value)
        assert _v_ == get_nvml().NVML_SUCCESS

    def get_speed(self):
        return self.get_pwm()


def get_fan_speed_at_pwm(pwm, fan):
    speeds = []
    fan.set_pwm(pwm)

    for _ in range(21):
        time.sleep(1)
        speed = fan.get_speed()
        speeds.append(speed)
        logging.debug(f"{speed} rpm")

    speeds.sort()
    return speeds[10]


def get_fan_speeds(fan, n=7):
    speeds = {}
    for pwm in numpy.linspace(0, 255, n):
        pwm = int(pwm)
        logging.info(f"pwm: {pwm}")
        speeds[pwm] = get_fan_speed_at_pwm(pwm, fan)

    # improve resolution at 0 rpm
    pwms = iter(sorted(speeds.keys()))
    pwm = next(pwms)

    if speeds[pwm] == 0:
        previous_pwm = pwm
        for pwm in pwms:
            if speeds[pwm] != 0:
                break

            previous_pwm = pwm

        else:
            pwm = None

        if pwm is not None:
            for _ in range(int(math.log2(n))):
                next_pwm = (previous_pwm + pwm) // 2
                if next_pwm in speeds.keys():
                    break

                logging.info(f"pwm: {next_pwm}")
                speed = get_fan_speed_at_pwm(next_pwm, fan)
                speeds[next_pwm] = speed

                if speed == 0:
                    previous_pwm = next_pwm

                else:
                    pwm = next_pwm

    return speeds


class Interpolator:
    def __init__(self, xs, ys, with_zeros=False, without_fallback=False):
        super().__init__()

        self.xs = xs
        self.ys = ys
        self.with_zeros = with_zeros
        self.without_fallback = without_fallback

        xs = numpy.asarray(xs)
        ys = numpy.asarray(ys, dtype=float)

        # sort
        indices = numpy.argsort(xs)
        xs = xs[indices]
        ys = ys[indices]

        # average decreasing points
        while True:
            iterator = iter(enumerate(ys))
            _, previous_y = next(iterator)

            for index, y in iterator:
                if y < previous_y:
                    y = (previous_y + y) / 2
                    ys[index - 1] = y
                    ys[index] = y
                    break

                previous_y = y

            else:
                break

        self._get_fallback_y = s_interpolate.interp1d(
            xs, ys, kind="linear", bounds_error=False, fill_value=(ys[0], ys[-1])
        )

        # remove 0
        if not with_zeros:
            non_zero = ys != 0
            xs = xs[non_zero]
            ys = ys[non_zero]

        self._min_x = xs[0]
        self._max_x = xs[-1]
        self._get_y = s_interpolate.interp1d(xs, ys, kind="quadratic")

        # remove difficult endpoints
        if not without_fallback:
            left = xs[:-1]
            right = xs[1:]
            left_xs = left * 0.99 + right * 0.01
            right_xs = left * 0.01 + right * 0.99

            while True:
                _ys = self._get_y([left_xs[0], right_xs[0]])
                if ((ys[0] <= _ys) & (_ys <= ys[1])).all():
                    break

                xs = xs[1:]
                ys = ys[1:]
                left_xs = left_xs[1:]
                right_xs = right_xs[1:]
                self._min_x = xs[0]
                self._get_y = s_interpolate.interp1d(xs, ys, kind="quadratic")

            while True:
                _ys = self._get_y([left_xs[-1], right_xs[-1]])
                if ((ys[-2] <= _ys) & (_ys <= ys[-1])).all():
                    break

                xs = xs[:-1]
                ys = ys[:-1]
                left_xs = left_xs[:-1]
                right_xs = right_xs[:-1]
                self._max_x = xs[-1]
                self._get_y = s_interpolate.interp1d(xs, ys, kind="quadratic")

        left = xs[:-1]
        right = xs[1:]
        left_xs = left * 0.99 + right * 0.01
        right_xs = left * 0.01 + right * 0.99

        left_ys = self.get(left_xs)
        right_ys = self.get(right_xs)
        left = ys[:-1]
        right = ys[1:]

        _v_ = (left <= left_ys) & (left_ys <= right) & (left <= right_ys)
        self._is_monotonic = (_v_ & (right_ys <= right)).all()

    def get(self, xs):
        xs = numpy.asarray(xs)
        ys = numpy.zeros(shape=(len(xs),), dtype=float)

        inside = (self._min_x <= xs) & (xs <= self._max_x)
        ys[inside] = self._get_y(xs[inside])
        ys[~inside] = self._get_fallback_y(xs[~inside])

        return ys


if __name__ == "__main__":
    import nestedtext
    import pydantic
    import argparse
    import pickle

    class SensorsConfiguration(pydantic.BaseModel):
        name: str
        group: str
        number: int
        sensor_group: str
        sensor_name: str
        control_points: dict[int, int]
        fan_speeds: dict[int, int] | None = None
        window_width: int = 1
        window_function: typing.Literal["min", "mean", "median", "max"] = "mean"

    class NvidiaConfiguration(pydantic.BaseModel):
        name: str
        gpu: str
        index: int
        control_points: dict[int, int]
        fan_speeds: dict[int, int] | None = None
        window_width: int = 1
        window_function: typing.Literal["min", "mean", "median", "max"] = "mean"

    class Configuration(pydantic.BaseModel):
        sensors: list[SensorsConfiguration] | None = None
        nvidia: list[NvidiaConfiguration] | None = None

    class Object:  # see me lazy
        pass

    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest="command", required=True)

    base_parser = argparse.ArgumentParser(add_help=False)
    base_parser.add_argument("--log_level", default="INFO")

    subparsers.add_parser("list", parents=[base_parser])

    plot_parser = subparsers.add_parser("plot", parents=[base_parser])
    plot_parser.add_argument("config")
    plot_parser.add_argument("--with_zeros", default=False, action="store_true")
    plot_parser.add_argument("--without_fallback", default=False, action="store_true")

    run_parser = subparsers.add_parser("run", parents=[base_parser])
    run_parser.add_argument("config")

    arguments = parser.parse_args()

    logging.basicConfig(level=arguments.log_level)

    objects = []
    if arguments.command in ["plot", "run"]:
        configuration = Configuration.model_validate(nestedtext.load(arguments.config))
        functions = dict(
            min=min,
            mean=lambda iterator: numpy.mean(list(iterator)),
            median=lambda iterator: numpy.median(list(iterator)),
            max=max,
        )
        for fan_configuration in itertools.chain(
            [] if configuration.sensors is None else configuration.sensors,
            [] if configuration.nvidia is None else configuration.nvidia,
        ):
            _v_ = functions[fan_configuration.window_function]
            fan_configuration._window_function = _v_

        _v_ = SensorsGroup.get_all()
        _v_ = {sensors_group.get_name(): sensors_group for sensors_group in _v_}
        sensors_groups = _v_

        nvidia_devices = {}
        if pynvml is not None:
            _v_ = NvidiaDevice.get_all()
            _v_ = ((nvidia_device.get_name(), nvidia_device) for nvidia_device in _v_)
            nvidia_devices.update(_v_)

        for fan_configuration in itertools.chain(
            [] if configuration.sensors is None else configuration.sensors,
            [] if configuration.nvidia is None else configuration.nvidia,
        ):
            match fan_configuration:
                case SensorsConfiguration():
                    _v_ = sensors_groups[fan_configuration.sensor_group].get_sensors()
                    (sensor,) = (
                        sensors_sensor
                        for sensors_sensor in _v_
                        if sensors_sensor.get_name() == fan_configuration.sensor_name
                    )

                    _v_ = sensors_groups[fan_configuration.group].get_fans()
                    (fan,) = (
                        sensors_fan
                        for sensors_fan in _v_
                        if sensors_fan.is_pwm
                        and sensors_fan.number == fan_configuration.number
                    )

                    dictionary_path = pathlib.Path(
                        f"sensors.{fan_configuration.group}.{fan_configuration.number}"
                    )

                case NvidiaConfiguration():
                    nvidia_device = nvidia_devices[fan_configuration.gpu]
                    sensor = nvidia_device
                    (fan,) = (
                        nvidia_fan
                        for nvidia_fan in nvidia_device.get_fans()
                        if nvidia_fan.index == fan_configuration.index
                    )
                    dictionary_path = pathlib.Path(f"nvidia.{fan_configuration.index}")

            if dictionary_path.exists():
                with open(dictionary_path, "rb") as file:
                    fan_dictionary = pickle.load(file)

                if fan_configuration.fan_speeds is not None:
                    fan_dictionary["speeds"] = fan_configuration.fan_speeds

            else:
                fan_dictionary = {}

                if fan_configuration.fan_speeds is None:
                    logging.info(f"getting fan speeds for fan {repr(fan.get_id())}")
                    fan_pwm = fan.get_pwm()
                    try:
                        with pwm_enable(fan, MANUAL_MODE):
                            fan_dictionary["speeds"] = get_fan_speeds(fan)

                    finally:
                        fan.set_pwm(fan_pwm)

                    with open(dictionary_path, "wb") as file:
                        pickle.dump(fan_dictionary, file)

                else:
                    fan_dictionary["speeds"] = fan_configuration.fan_speeds

            temperatures, speeds = zip(*fan_configuration.control_points.items())

            _v_ = (min(speeds), max(speeds))
            get_speed = s_interpolate.interp1d(
                temperatures, speeds, kind="linear", bounds_error=False, fill_value=_v_
            )

            speeds = fan_dictionary["speeds"]
            pwms = sorted(speeds.keys())
            speeds = [speeds[pwm] for pwm in pwms]

            _1 = arguments.with_zeros if arguments.command == "plot" else False
            _2 = arguments.without_fallback if arguments.command == "plot" else False
            _v_ = Interpolator(pwms, speeds, with_zeros=_1, without_fallback=_2)
            interpolator = _v_

            pwms = numpy.arange(0, 255 + 1)
            speeds = interpolator.get(pwms)
            indices = numpy.argsort(speeds, kind="stable")
            speeds = speeds[indices]
            pwms = pwms[indices]
            get_pwms = s_interpolate.interp1d(
                speeds, pwms, kind="linear", bounds_error=False, fill_value=(0, 255)
            )

            object = Object()
            object.fan_configuration = fan_configuration
            object.sensor = sensor
            object.get_speed = get_speed
            object.interpolator = interpolator
            object.get_pwms = get_pwms
            object.fan = fan
            object.fan_dictionary = fan_dictionary
            object.min_speed = min(speeds)
            object.max_speed = max(speeds)

            objects.append(object)

    if arguments.command == "list":
        print("sensors:")
        for sensors_group in SensorsGroup.get_all():
            print(f"  {repr(sensors_group.get_name())}")

            _v_ = f"    sensors: {repr([sensors_sensor.get_name() for sensors_sensor in sensors_group.get_sensors()])}"
            print(_v_)

            _v_ = f"    fans: {repr([sensors_fan.number for sensors_fan in sensors_group.get_fans() if sensors_fan.is_pwm])}"
            print(_v_)

        if pynvml is not None:
            print("nvidia:")
            for nvidia_device in NvidiaDevice.get_all():
                print(f"  {repr(nvidia_device.get_name())}")

                _v_ = f"    fans: {repr([nvidia_fan.index for nvidia_fan in nvidia_device.get_fans()])}"
                print(_v_)

    if arguments.command == "plot":
        import bokeh.layouts as b_layouts
        import bokeh.palettes as b_palettes
        import bokeh.plotting as b_plotting

        figures = []

        figure = b_plotting.figure(x_axis_label="temperature", y_axis_label="speed")

        for object, color in zip(objects, b_palettes.Category20[20]):
            name = object.fan.get_id()

            temperatures = numpy.arange(0, 100 + 1)
            speeds = object.get_speed(temperatures)
            figure.line(temperatures, speeds, color=color, legend_label=name)

            control_points = object.fan_configuration.control_points
            temperatures = list(control_points.keys())
            speeds = [control_points[temperature] for temperature in temperatures]
            figure.scatter(temperatures, speeds, color=color, legend_label=name)

        figure.x_range.start = 0
        figure.y_range.start = 0
        figure.legend.location = "top_left"
        figure.legend.click_policy = "hide"

        figures.append(figure)

        title = f"with zeros: {arguments.with_zeros}, without fallback: {arguments.without_fallback}"

        _v_ = b_plotting.figure(title=title, x_axis_label="speed", y_axis_label="pwm")
        figure = _v_

        for object, color in zip(objects, b_palettes.Category20[20]):
            name = object.fan.get_id()

            speeds = numpy.linspace(object.min_speed, object.max_speed, 1001)
            pwms = object.get_pwms(speeds)

            _v_ = "solid" if object.interpolator._is_monotonic else "dashed"
            figure.line(speeds, pwms, color=color, legend_label=name, line_dash=_v_)

            pwms, speeds = zip(*object.fan_dictionary["speeds"].items())
            figure.scatter(speeds, pwms, color=color, legend_label=name)

        figure.x_range.start = -100
        figure.y_range.start = -13
        figure.legend.location = "top_left"
        figure.legend.click_policy = "hide"

        figures.append(figure)

        figure = b_plotting.figure(
            title=title, x_axis_label="temperature", y_axis_label="pwm"
        )

        for object, color in zip(objects, b_palettes.Category20[20]):
            name = object.fan.get_id()

            temperatures = numpy.arange(0, 100 + 1)
            speeds = object.get_speed(temperatures)
            pwms = object.get_pwms(speeds)

            _v_ = "solid" if object.interpolator._is_monotonic else "dashed"
            figure.line(
                temperatures, pwms, color=color, legend_label=name, line_dash=_v_
            )

        figure.y_range.start = -13
        figure.legend.location = "top_left"
        figure.legend.click_policy = "hide"

        figures.append(figure)

        figure = b_layouts.column(figures)

        b_plotting.show(figure)

    elif arguments.command == "run":
        for object in objects:
            if not object.interpolator._is_monotonic:
                _v_ = repr(object.fan.get_id())
                raise Exception(f"fan curve is not monotonic; fan name: {_v_}")

        logging.info("starting control loop")

        try:
            with contextlib.ExitStack() as exit_stack:
                for object in objects:
                    exit_stack.enter_context(pwm_enable(object.fan, MANUAL_MODE))

                previous_pwms = [-1] * len(objects)
                temperature_deques = [
                    collections.deque(maxlen=object.fan_configuration.window_width)
                    for object in objects
                ]
                count = 0

                while True:
                    time.sleep(1)

                    _v_ = enumerate(zip(objects, previous_pwms, temperature_deques))
                    for index, (object, previous_pwm, temperature_deque) in _v_:
                        temperature_deque.appendleft(object.sensor.get_temperature())

                        _v_ = range(object.fan_configuration.window_width)
                        _v_ = zip(_v_, temperature_deque)
                        _v_ = (temperature for _, temperature in _v_)
                        temperature = object.fan_configuration._window_function(_v_)

                        speed = object.get_speed(temperature)
                        pwm = round(one(object.get_pwms([speed])))

                        if pwm == previous_pwm:
                            continue

                        if logging.getLogger().level <= logging.DEBUG:
                            logging.debug(
                                f"fan: {repr(object.fan.get_id())}|csp:"
                                f" {object.fan.get_speed():4.0f}|tmp:"
                                f" {temperature:5.2f}|tsp: {speed:4.0f}|pwm: {pwm:3}"
                            )

                        object.fan.set_pwm(pwm)
                        previous_pwms[index] = pwm

                    count += 1

                    # set PWM mode to MANUAL every minute
                    if count % 60 == 0:
                        for object in objects:
                            object.fan.set_pwm_enable(MANUAL_MODE)

        finally:
            for object in objects:
                object.fan.set_pwm(128)
