# Fan speed control

## What it is NOT!

- A trustworthy alternative to control your fans
  - it might work, but it might as well not work as intended and harm your hardware
- An optimal approach
  - many aspects of fan speed control are completely ignored

## What it is

- The story of a random guy thinking "why is fan speed control so static, there must be a better way"
- A Python script for the curious or bold

## Path

For many years I relied on *[fancontrol](https://github.com/lm-sensors/lm-sensors)* with a base speed of 800 rpm and a steep slope to max speed near Tmax\*. The rationale: as long as temperatures are well below Tmax, there is no need to adjust fan speeds. Temperatures rise and fall depending on system load and tend to stay well below Tmax. Occasionally, when system load remains high or ambient temperature is high, temperatures would reach the bottom of the slope and fans would spin up for a few seconds every few seconds due to the steep slope. With only one linear response to temperature available, this problem can't be solved with *fancontrol* without sacrifice.

Then I found *[fan2go](https://github.com/markusressel/fan2go/issues)*. It provides much more sophisticated control over the temperature response and adjusts fan pwm using PID controller logic. By looking at the code I found constant parameters for the PID and thought "are PID controlled pwm and those particular parameters optimal for my goal? Probably not".

As statistician by education I always look for solutions based on statistical methods. I realized that a necessary condition for "optimal" fan speed control is to predict thermal energy emission. Modern hardware measure power draw which opens a deterministic window into the future because temperature can be seen as an accumulative process. Then again, assuming we had an appropriate model and enough data to estimate all unknowns, what would we gain? Under certain assumptions, simple methods should work reasonably well.

That is, if temperature response to power draw is slow compared to fan speed response to temperature, then simply setting fan speed based on current temperature is sufficient to prevent overheating. Most often this is the case and probably the reason why most methods for fan speed control are static. Why strive for an expensive solution which is optimal when there is a cheap solution that is good enough.

Eventually, with a few prototypes that read temperatures, get/set fan speeds, estimate fan speed for a given pwm and estimate fan speeds for given (pwm, speed) pairs developed along the way, I put it all together into this Python script.

\* the maximum temperature below which the hardware won't suffer from accelerated wear, provided by the manufacturer

## Conclusion

Sometimes the path is more important than the result. The Python script certainly achieves my goal, but it doesn't control fans any better than *fancontrol*. If *fancontrol* would allow multiple control points, I'd use it, no doubt.

For the curious or bold, it is probably worth looking at the code.
